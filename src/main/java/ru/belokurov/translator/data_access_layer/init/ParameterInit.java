package ru.belokurov.translator.data_access_layer.init;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import ru.belokurov.translator.data_access_layer.model.Parameter;
import ru.belokurov.translator.data_access_layer.repository.ParameterRepository;

import java.util.ArrayList;
import java.util.List;

@Component
public class ParameterInit implements ApplicationRunner
{

    ParameterRepository parameterRepository;

    public ParameterInit(ParameterRepository parameterRepository) {
        this.parameterRepository = parameterRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<Parameter> parameterList = new ArrayList<>();
        parameterList.add(new Parameter(Parameter.EnumParameters.EN_RU));
        parameterList.add(new Parameter(Parameter.EnumParameters.RU_EN));
        parameterRepository.saveAll(parameterList);
    }
}

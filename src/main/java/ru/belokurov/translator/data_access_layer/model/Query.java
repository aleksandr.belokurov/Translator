package ru.belokurov.translator.data_access_layer.model;


import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "query")
@Getter
@Setter
@NoArgsConstructor
public class Query {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "input_data")
    private String inputData;

    @Column(name = "output_data")
    private String outputData;

    @ManyToMany(cascade = { CascadeType.MERGE })
    @JoinTable(
            name = "Query_Parametr",
            joinColumns = { @JoinColumn(name = "query_id") },
            inverseJoinColumns = { @JoinColumn(name = "parameter_id") }
    )
    Set<Parameter> parameters = new HashSet<>();

    @OneToMany( mappedBy = "query",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Word> words = new ArrayList<>();;

    @Column(name = "ip_address")
    private String ipAddress;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    public Query(String inputData, String outputData, Set<Parameter> parameters, String ipAddress) {
        this.inputData = inputData;
        this.outputData = outputData;
        this.parameters = parameters;
        this.ipAddress = ipAddress;
    }

}

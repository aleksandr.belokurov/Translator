package ru.belokurov.translator.data_access_layer.model;


import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "word")
@Getter
@NoArgsConstructor
public class Word {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "input_word")
    private String inputWord;

    @Column(name = "output_word")
    private String outputWord;

    @ManyToOne(fetch = FetchType.LAZY)
    private Query query;

    public Word(String inputWord, String outputWord) {
        this.inputWord = inputWord;
        this.outputWord = outputWord;
    }

    public void setQuery(Query query) {
        this.query = query;
    }
}

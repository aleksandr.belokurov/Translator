package ru.belokurov.translator.data_access_layer.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "parameter")
@Getter
@NoArgsConstructor
@EqualsAndHashCode
public class Parameter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
    @Column(name = "parameter")
    private EnumParameters parameter;

    @ManyToMany(mappedBy = "parameters")
    @ToString.Exclude
    private Set<Query> queries = new HashSet<>();

    public Parameter(EnumParameters parameter) {
        this.parameter = parameter;
    }

    @Getter
    public enum EnumParameters {

        EN_RU("en", "ru"),
        RU_EN("ru", "en")
        ;

        EnumParameters(String inputLanguage, String outputLanguage) {
            this.inputLanguage = inputLanguage;
            this.outputLanguage = outputLanguage;
        }


        private final String inputLanguage;
        private final String outputLanguage;


    }

}

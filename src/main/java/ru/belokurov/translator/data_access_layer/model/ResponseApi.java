package ru.belokurov.translator.data_access_layer.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


public class ResponseApi {

    private final String translatedText;
    private final String detectedSourceLanguage;

    public ResponseApi(String translatedText, String detectedSourceLanguage) {
        this.translatedText = translatedText;
        this.detectedSourceLanguage = detectedSourceLanguage;
    }


}

package ru.belokurov.translator.data_access_layer.repository;

import org.springframework.data.repository.CrudRepository;
import ru.belokurov.translator.data_access_layer.model.Word;

public interface WordRepository extends CrudRepository<Word, Long> {

}

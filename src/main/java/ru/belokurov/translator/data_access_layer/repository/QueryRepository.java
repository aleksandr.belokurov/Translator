package ru.belokurov.translator.data_access_layer.repository;

import org.springframework.data.repository.CrudRepository;
import ru.belokurov.translator.data_access_layer.model.Query;

public interface QueryRepository extends CrudRepository<Query, Long> {

}

package ru.belokurov.translator.data_access_layer.repository;

import org.springframework.data.repository.CrudRepository;
import ru.belokurov.translator.data_access_layer.model.Parameter;

import java.util.List;

public interface ParameterRepository extends CrudRepository<Parameter, Long> {
    List<Parameter> findByParameter(Parameter.EnumParameters parameter);

}

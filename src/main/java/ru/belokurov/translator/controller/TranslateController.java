package ru.belokurov.translator.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.belokurov.translator.data_access_layer.model.Parameter;
import ru.belokurov.translator.data_access_layer.model.Query;
import ru.belokurov.translator.data_access_layer.model.Word;
import ru.belokurov.translator.service.*;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;


@RestController
@RequestMapping("/translate")
@AllArgsConstructor
public class TranslateController {

    private final QueryService queryService;
    private final TranslateService translateService;
    private final ParameterService parameterService;
    private final WordService wordService;
    private final ClientInfoService clientInfoService;



    @PostMapping
    @ResponseBody
    public String saveQuery(
                            @RequestParam("sentence") String text,
                          @RequestParam("param") String strTranslateParam)
            throws IOException, InterruptedException {
        Parameter.EnumParameters translateParam = Parameter.EnumParameters.valueOf(strTranslateParam);
        String translateSentence = translateService.translate(text, translateParam.getOutputLanguage());
        List<Word> wordList = wordService.getTranslateWords(text, translateParam);
        Query query = new Query(text, translateSentence,
                new HashSet<>(parameterService.findByParameter(translateParam)), clientInfoService.getIp());
        wordList.forEach((e) -> e.setQuery(query));
        query.setWords(wordList);
        queryService.save(query);
        return translateSentence;
    }

}

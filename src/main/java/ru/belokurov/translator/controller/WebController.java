package ru.belokurov.translator.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.belokurov.translator.data_access_layer.model.Parameter;
import java.util.Arrays;
import java.util.List;

@Controller
public class WebController {

    @GetMapping("/")
    public String home(Model model) {
        List<Parameter.EnumParameters> list = Arrays.asList(Parameter.EnumParameters.values());
        model.addAttribute("paramTranslate", list);
        return "index";
    }


}

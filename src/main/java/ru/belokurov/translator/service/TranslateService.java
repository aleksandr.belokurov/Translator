package ru.belokurov.translator.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import ru.belokurov.translator.data_access_layer.model.Word;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class TranslateService {

    public String translate(String sentence, String paramLanguage) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://deep-translate1.p.rapidapi.com/language/translate/v2"))
                .header("content-type", "application/json")
                .header("X-RapidAPI-Host", "deep-translate1.p.rapidapi.com")
                .header("X-RapidAPI-Key", "47a52bb1bdmsh3028475aaebb810p11873djsn8c06558ce712")
                .method("POST", HttpRequest.BodyPublishers.ofString("{\r \"q\": \" "
                        + sentence +" \",\r \"target\": \"" + paramLanguage + "\"\r }")).build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        JsonNode jsonNode = new ObjectMapper().readTree(response.body());
        jsonNode = jsonNode.get("data");
        jsonNode = jsonNode.get("translations");
        String result = jsonNode.get("translatedText").asText();
        return result;
    }

    public Word translateWord(String str, String paramLanguage) throws IOException, InterruptedException {
        return new Word(str, translate(str, paramLanguage));
    }


}

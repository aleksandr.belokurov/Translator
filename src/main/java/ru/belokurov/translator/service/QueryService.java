package ru.belokurov.translator.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.belokurov.translator.data_access_layer.model.Query;
import ru.belokurov.translator.data_access_layer.repository.QueryRepository;


@AllArgsConstructor
@Service
public class QueryService {
    private QueryRepository queryRepository;

    public void save(Query query) {
        queryRepository.save(query);
    }

}

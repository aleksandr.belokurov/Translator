package ru.belokurov.translator.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.belokurov.translator.data_access_layer.model.Parameter;
import ru.belokurov.translator.data_access_layer.repository.ParameterRepository;

import java.util.List;

@AllArgsConstructor
@Service
public class ParameterService {

    private final ParameterRepository parameterRepository;

    public List<Parameter> findByParameter(Parameter.EnumParameters parameter) {
        return parameterRepository.findByParameter(parameter);
    }


}

package ru.belokurov.translator.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.belokurov.translator.data_access_layer.model.Parameter;
import ru.belokurov.translator.data_access_layer.model.Word;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class WordService {

    private final TranslateService translateService;

    public List<String> parseSentence(String sentence) {
        String[] result = sentence.split("[ ]+");
        return Arrays.asList(result);
    }

    public List<Word> getTranslateWords(String text, Parameter.EnumParameters translateParam) {
        return this.parseSentence(text).
                parallelStream().map((e) -> {
                    try {
                        return translateService.translateWord(e, translateParam.getOutputLanguage());
                    } catch (IOException | InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    return null;
                }).collect(Collectors.toList());
    }


}
